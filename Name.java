import java.util.Scanner;


public class Chapter2 {

	private static Scanner keyboard;
	
	public static void main(String[] args) {
		
		String s1, s2, s3, s4;
		
	    	keyboard = new Scanner(System.in);

		System.out.println ("Enter a favorite color");
		s1= keyboard.next();
		
		System.out.println("Enter a favorite food");
		s2= keyboard.next();
		
		System.out.println("Enter the first name of a friend or relative");
		s3= keyboard.next();
		
		System.out.println("Enter a favorite animal");
		s4= keyboard.next();
		
		System.out.println("I had a dream that" + s3+ " "+ "ate a"+ s1+ " " + s4);
		System.out.println("and said it tasted like "+ s2+ "!");
	}

}